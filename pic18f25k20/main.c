#include <main.h>
#include "dui.c"

//RFID
// unsigned int16 rfid_p0min = 110;
// unsigned int16 rfid_p0max = 250;
// unsigned int16 rfid_p1min = 280;
// unsigned int16 rfid_p1max = 380;
unsigned int16 rfid_p0min = 110;
unsigned int16 rfid_p0max = 210;
unsigned int16 rfid_p1min = 270;
unsigned int16 rfid_p1max = 371;
unsigned int16 rfid_period = 0;

unsigned int8 rfid_state = 0;
unsigned int8 rfid_prcount = 0;
unsigned int8 rfid_datacount = 0;
unsigned int8 rfid_databit = 0;
unsigned int8 rfid_raw[16];
unsigned int8 rfid_tetrad[11];

unsigned int16 rfid_version = 0;
unsigned int8 rfid_serial = 0;
unsigned int16 rfid_number = 0;
unsigned int8 rfid_check = 0;

//TASK RFID CHECK
#task(rate = 10 ms, max = 300 us)
void task_RFID();

void rfid_toTeatrads();
void rfif_toNumbers();


//TASK MESSAGE CHECK
#task(rate = 1 ms, max = 300 us)
void task_DUIcheck();

void formInit(void);
void formRead(unsigned int8 channel, unsigned int8* data);

//-------------------------------------------------

//implementation
void initialization() {
    SET_TRIS_A( 0x00 );
    SET_TRIS_B( 0x03 );
    SET_TRIS_C( 0x84 );
    DUIInit();
    DUIOnFormInit = formInit;
    DUIOnReadData = formRead;
    output_low(PWM);
    output_low(PIN_A1);
    output_low(PIN_A2);
    output_low(PIN_A3);

    //pwm
    setup_timer_2(T2_DIV_BY_1,39,1);
    setup_ccp1(CCP_PWM|CCP_SHUTDOWN_AC_L|CCP_SHUTDOWN_BD_L);
    set_pwm1_duty((int16)78);
    //decoder
    setup_timer_3(T3_INTERNAL | T3_DIV_BY_8);
    enable_interrupts(INT_EXT);
    ext_int_edge(H_TO_L);
    enable_interrupts(GLOBAL);

}

#INT_EXT
void  EXT_isr(void) 
{
    if (rfid_state == 1 && rfid_datacount > 15) return;
    rfid_period = get_timer3();
    if (!input(pINT)) { //H2L
        if (rfid_period > rfid_p1min && rfid_period < rfid_p1max) {
            output_bit(LED2, 1);
            if (rfid_state == 0) {
                if (rfid_prcount == 0) rfid_prcount ++;
                else rfid_prcount = 0;
            }  else {
                rfid_raw[rfid_datacount] = rfid_raw[rfid_datacount] << 1;
                rfid_databit ++;
                if (rfid_databit == 8) {
                    rfid_databit = 0;
                    rfid_datacount ++;
                }
                rfid_raw[rfid_datacount] = rfid_raw[rfid_datacount] << 1;
                rfid_databit ++;
                if (rfid_databit == 8) {
                    rfid_databit = 0;
                    rfid_datacount ++;
                }
            }
        } else if (rfid_period > rfid_p0min && rfid_period < rfid_p0max) {
            output_bit(LED3, 1);
            if (rfid_state == 0) {
                if (rfid_prcount > 0 && rfid_prcount < 17)  rfid_prcount ++;
            } else {
                rfid_raw[rfid_datacount] = rfid_raw[rfid_datacount] << 1;
                rfid_databit ++;
                if (rfid_databit == 8) {
                    rfid_databit = 0;
                    rfid_datacount ++;
                }
            }
        } else {
            if (rfid_state == 0) rfid_prcount = 0;
            else rfid_state = 0;
        }

        //ext_int_edge(H_TO_L);
        ext_int_edge(L_TO_H);
    } else {
        if (rfid_period > rfid_p1min && rfid_period < rfid_p1max) {

            if (rfid_state == 0) {
                if (rfid_prcount > 0 && rfid_prcount < 17)  rfid_prcount = 0;
                if (rfid_prcount == 17) {
                    rfid_state = 1;
                    rfid_prcount = 0;
                    rfid_datacount = 0;
                    rfid_databit = 0;
                    rfid_raw[rfid_datacount] = rfid_raw[rfid_datacount] << 1;
                    rfid_raw[rfid_datacount] ++;
                    rfid_databit ++;
                }
            } else {
                rfid_raw[rfid_datacount] = rfid_raw[rfid_datacount] << 1;
                rfid_raw[rfid_datacount] ++;
                rfid_databit ++;
                if (rfid_databit == 8) {
                    rfid_databit = 0;
                    rfid_datacount ++;
                }
                rfid_raw[rfid_datacount] = rfid_raw[rfid_datacount] << 1;
                rfid_raw[rfid_datacount] ++;
                rfid_databit ++;
                if (rfid_databit == 8) {
                    rfid_databit = 0;
                    rfid_datacount ++;
                }
            }
        } else
            if (rfid_period > rfid_p0min && rfid_period < rfid_p0max) {

                if (rfid_state == 0) {
                    if (rfid_prcount > 0 && rfid_prcount < 16) rfid_prcount ++;
                }  else {
                    rfid_raw[rfid_datacount] = rfid_raw[rfid_datacount] << 1;
                    rfid_raw[rfid_datacount] ++;
                    rfid_databit ++;
                    if (rfid_databit == 8) {
                        rfid_databit = 0;
                        rfid_datacount ++;
                    }
                }
            }  else {
                if (rfid_state == 0) rfid_prcount = 0;
                else rfid_state = 0;
            }
        ext_int_edge(H_TO_L);
        output_bit(LED2, 0);
        output_bit(LED3, 0);
    }
    set_timer3(0);
}

//TASK RFID CHECK
void task_RFID() {
    if (rfid_state && rfid_datacount > 15) {
        rfid_state = 0;
        rfid_datacount = 0;
        rfid_databit = 0;
        rfid_prcount = 0;
        rfid_toTeatrads();
        if (!rfid_check) return;
        output_bit(LED1, 1);
        rfif_toNumbers();
        char string[40];
         DUISendUInt16(3, rfid_version);
         DUISendUInt8(4, rfid_serial);
         DUISendUInt16(5, rfid_number);
        sprintf(string,"Ver:%Lu Serial:%u Num:%Lu",
        rfid_version, 
        rfid_serial,
        rfid_number);
        DUISendLog (string); 
    } else output_bit(LED1, 0);
}

void rfid_toTeatrads() {
    int bt = 0;
    int bi = 0;
    unsigned int8 xorchk = 0;
    unsigned int1 chk = 0;
    unsigned int1 bit1;
    unsigned int1 bit2;
    unsigned int1 bit;
    rfid_check = 0;
    for (int i = 0; i < 11; i ++) {
        rfid_tetrad[i] = 0;
        chk = 0;
        for (int b = 0; b < 5; b++) {
            bit1 = ((rfid_raw[bt] & (0b10000000 >> bi)) != 0);
            bi ++;
            if (bi == 8) {bi = 0; bt ++; }
            bit2 = ((rfid_raw[bt] & (0b10000000 >> bi)) != 0);
            bi ++;
            if (bi == 8) {bi = 0; bt ++; }
            bit = (bit1 < bit2);
            if (b < 4) {
                rfid_tetrad[i] = rfid_tetrad[i] << 1;
                if (bit) rfid_tetrad[i] ++;
                chk ^= bit;
            } else if (i > 1 && i < 10) {
                rfid_check = rfid_check << 1;
                rfid_check += (chk != bit);
            }
        }
        if (i < 10) xorchk ^= rfid_tetrad[i];
    }

    rfid_check = (xorchk == rfid_tetrad[10] && rfid_check == 0);
}

void rfif_toNumbers() {
    rfid_version = rfid_tetrad[0];
    rfid_version = rfid_version << 4;
    rfid_version += rfid_tetrad[1];

    rfid_serial = rfid_tetrad[2];
    rfid_serial = rfid_serial << 4;
    rfid_serial += rfid_tetrad[3];
    rfid_serial = rfid_serial << 4;
    rfid_serial += rfid_tetrad[4];
    rfid_serial = rfid_serial << 4;
    rfid_serial += rfid_tetrad[5];

    rfid_number = rfid_tetrad[6];
    rfid_number = rfid_number << 4;
    rfid_number += rfid_tetrad[7];
    rfid_number = rfid_number << 4;
    rfid_number += rfid_tetrad[8];
    rfid_number = rfid_number << 4;
    rfid_number += rfid_tetrad[9];
}


void task_DUIcheck() {
    DUICheck();
    restart_wdt();
}

void formInit(void) {
    char suffix[]="";
    char text3[]="version";
    DUIAddLabel(3, text3, suffix);
    char text4[]="serial";
    DUIAddLabel(4, text4, suffix);
    char text5[]="number";
    DUIAddLabel(5, text5, suffix);

}

void formRead(unsigned int8 channel, unsigned int8* data) {

}

void main()
{
    initialization();
    rtos_run();
}
